import cv2
import numpy as np 
import os
import glob
import pandas as pd 

y_train = [cv2.imread(file, cv2.IMREAD_GRAYSCALE) for file in glob.glob("Diaretdb1/Train/blocks/y_train/*.png")]
y_train = [int(bool(np.where(img > 188)[0].size)) for img in y_train]

np.savetxt('DB/y_train.csv', y_train, delimiter=',', fmt='%d')