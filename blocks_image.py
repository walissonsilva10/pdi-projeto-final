import cv2
import numpy as np 
import os
import glob

path = "Diaretdb1/Test"
names_path = os.listdir(path)

path_x = path + "/" + names_path[1]
path_y = path + "/" + names_path[2]
label_x = names_path[1]
label_y = names_path[2]

file_x = glob.glob(path_x + "/*.png")
file_y = glob.glob(path_y + "/*.png")

file_x.sort()
file_y.sort()

for i in range(len(file_x)):
	print(file_x[i])
	print(file_y[i])
	img = cv2.imread(file_x[i])
	laudo = cv2.imread(file_y[i])

	rows, cols = img.shape[0], img.shape[1]

	kernel = 100
	block_image_x = np.zeros((kernel, kernel, 3))
	block_image_y = np.zeros((kernel, kernel, 3))

	block_it = 1

	for x in range(0, rows - kernel, kernel):
		for y in range(0, cols - kernel, kernel):
			block_image_x = img[x:x+kernel, y:y+kernel]
			block_image_y = laudo[x:x+kernel, y:y+kernel]

			cv2.imwrite(path_x + "/blocks/" + str(i + 1) + '_' + str(block_it) + '.png', block_image_x)
			cv2.imwrite(path_y + "/blocks/" + str(i + 1) + '_' + str(block_it) + '.png', block_image_y)
			block_it += 1