import cv2
import numpy as np
import os
import glob
import mahotas as mt
import time
import csv

def extract_features(image):
	# calculate haralick texture features for 4 types of adjacency
	textures = mt.features.haralick(image)

	# take the mean of it and return it
	ht_mean = textures.mean(axis=0)
	return ht_mean

# load the training dataset
train_path = "Diaretdb1/Train"
train_names = os.listdir(train_path)

# empty list to hold feature vectors and train labels
train_features = []
train_labels = []

with open('database.csv', mode='w') as employee_file:
	employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

	# loop over the training dataset
	print("[STATUS] Started extracting haralick textures..")
	path_x = train_path + "/" + train_names[0]
	path_y = train_path + "/" + train_names[1]
	label_x = train_names[0]
	label_y = train_names[1]

	file_x = glob.glob(path_x + "/*.png")
	file_y = glob.glob(path_y + "/*.png")

	for i in range(len(file_x)):
		print("Processing Image - {} in {}".format(i + 1, label_x))
		# read the training image
		image = cv2.imread(file_x[i])
		laudo = cv2.imread(file_y[i], cv2.IMREAD_GRAYSCALE)

		image = cv2.resize(image, (image.shape[0] // 2, image.shape[1] // 2), interpolation=cv2.INTER_LINEAR)
		laudo = cv2.resize(laudo, (laudo.shape[0] // 2, laudo.shape[1] // 2), interpolation=cv2.INTER_LINEAR)

		mask = 15
		piece_image = np.zeros((mask, mask, 3))
		rows, cols = image.shape[0], image.shape[1]

		time_start = time.time()

		for x in range(mask, rows - mask, mask):
			for y in range(mask, cols - mask, mask):
				piece_image = image[x:x+mask,y:y+mask,:]

				# convert the image to grayscale
				gray = cv2.cvtColor(piece_image, cv2.COLOR_BGR2GRAY)

				if (gray[mask//2,mask//2] != 0):

					# extract haralick texture from the image
					features = extract_features(gray)

					if (laudo[x+mask//2,y+mask//2] == 252):
						employee_writer.writerow([str(piece_image[mask//2,mask//2,2]), str(piece_image[mask//2,mask//2,1]), str(piece_image[mask//2,mask//2,0]), str(features[0]), str(features[1]), str(features[2]), str(features[3]), str(features[4]), str(features[5]), str(features[6]), str(features[7]), str(features[8]), str(features[9]), str(features[10]), str(features[11]), str(features[12]), '1'])
					else:
						employee_writer.writerow([str(piece_image[mask//2,mask//2,2]), str(piece_image[mask//2,mask//2,1]), str(piece_image[mask//2,mask//2,0]), str(features[0]), str(features[1]), str(features[2]), str(features[3]), str(features[4]), str(features[5]), str(features[6]), str(features[7]), str(features[8]), str(features[9]), str(features[10]), str(features[11]), str(features[12]), '0'])
					
					# append the feature vector and label
					#train_features.append(features)
					#train_labels.append(cur_label)

					#print('BGR:', image[x, y])
					#print('Texture Features:', features)

					# show loop update
		print(time.time() - time_start)

# have a look at the size of our feature vector and labels
print("Training features: {}".format(np.array(train_features).shape))
print("Training labels: {}".format(np.array(train_labels).shape))