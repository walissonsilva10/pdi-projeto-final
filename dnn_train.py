# https://towardsdatascience.com/building-a-convolutional-neural-network-cnn-in-keras-329fbbadc5f5

from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten
import cv2
import numpy as np 
import os
import glob

print('Loading images...')
X_train = [cv2.imread(file) for file in glob.glob("Diaretdb1/Train/blocks/x_train/*.png")]
y_train = np.loadtxt('DB/y_train.csv', delimiter=',')
print('Images loaded.')

print('Pre-processing images...')
y_train.astype(int)
X_train = np.array(X_train)
print('Done.')

#create model
model = Sequential()

#add model layers
model.add(Conv2D(64, kernel_size=3, activation='relu', input_shape=(100,100,3)))
model.add(Conv2D(32, kernel_size=3, activation='relu'))
model.add(Flatten())
model.add(Dense(1, activation='softmax'))

#compile model using accuracy to measure model performance
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

#train the model
#model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10)
model.fit(X_train, y_train, epochs=10, batch_size=128)

scores = model.evaluate(X_train, y_train)

print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))